const express = require('express');
const path = require('path');
const cors = require('cors');
const app = express();
const bodyParser= require('body-parser');
const PORT = process.env.PORT || 3300;
const wooConfig= require('./src/wooConfig');
const WooCommerceAPI =require('woocommerce-api');

app.use(cors());
app.use(bodyParser.json());

const WooCommerce = new WooCommerceAPI({
	url: wooConfig.siteUrl,
	consumerKey: wooConfig.consumerKey,
	consumerSecret: wooConfig.consumerSecret,
	wpAPI: true,
	version: 'wc/v2'
});

app.get('/allProduct', function(req, res) {
  WooCommerce.get('products', function(err, data, response) {
    //console.log(response);
    res.send(response);
  });
});

app.post('/addProduct', function(req, res) {
  const product= req.body;
   console.log('product xxxxxxx', product);
  WooCommerce.post('products',product, function(err, data, response) {
    //console.log(response);
    res.send(response);
  });
});

app.post('/updateProduct/:id', function(req, res) {
  const product= req.body;
  const id=req.params.id;
   console.log('product xxxxxxx', id);
   WooCommerce.put(`products/${id}`, product, function(err, data, response) {
    res.send(response);
  });
  
});

app.delete('/delete/:id', function(req, res) {
  const id=req.params.id;
   console.log('product xxxxxxx', id);
   WooCommerce.delete(`products/${id}?force=true`, function(err, data, response) {
    res.send(response);
  });
  
});

app.listen(PORT, () => {
  console.log("xxxxxxx xxxxxxxxx server connnected to the port " + PORT);
})